<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\Mobile\Mobile;

$mobile = new Mobile();
$allTrashed= $mobile->trashed();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All trashed data</h2>
    <a href="index.php" class="btn btn-info" role="button">Go to List</a>
    <br>
    <br>
    <form action="recovermultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-info">Recover Multiple</button>
        <button type="button" class="btn btn-info" id="markButton">Delete  Multiple</button>
    <table class="table">
        <thead>
        <tr>
            <th>Select</th>
            <th>SL</th>
            <th>ID</th>
            <th>Book Title</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allTrashed as $trashed) {
            $sl++;
            if(($sl%2)==0){?>
        <tr class="success">
            <td><input type="checkbox" name="mark[]" value="<?php echo $trashed['id']?>"></td>
            <td><?php echo $sl ?></td>
            <td><?php echo $trashed['id'] ?></td>
            <td><?php echo $trashed['title'] ?></td>
            <td>
                <a href="recover.php?id=<?php echo $trashed['id']?>" class="btn btn-info" role="button">Recover</a>
                <a href="delete.php?id=<?php echo  $trashed['id']?>" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
              <?php }else {?>
                <tr class="danger">
                    <td><input type="checkbox" name="mark[]" value="<?php echo $trashed['id']?>"></td>
                    <td><?php echo $sl ?></td>
                    <td><?php echo $trashed['id'] ?></td>
                    <td><?php echo $trashed['title'] ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $trashed['id']?>" class="btn btn-info" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo  $trashed['id']?>" class="btn btn-danger" role="button">Delete</a>
                    </td>
                </tr>



        <?php }} ?>
        </tbody>
    </form>
    </table>

</div>
<script>
    $('#markButton').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });
</script>

</body>
</html>

